// Load in the main portion of the homepage
const initialiseHomePage = () => {
  $("#main-overlay").fadeTo(1000, 1);
  $("div#main-overlay-buttons").fadeTo(2000, 1);
};

// Animate Register/Login Sections
const initialiseAuthPages = () => {
  $("div.logout-in-card").animate(
    {
      marginTop: "100px",
      opacity: "1.0"
    },
    700
  );
};

$("document").ready(() => {
  initialiseHomePage();
  initialiseAuthPages();
});
