// Ensure assignments are added to the correct module
$(".teacher-add-assignment").on("click", function(e) {
  // Get the associated module of the button that was clicked
  const $button = $(this);
  const moduleID = $button.attr("module-id");
  // Set the hidden input value
  const hidden = $("#hidden-module-id");
  hidden.attr("value", moduleID);
});
