from django.contrib import admin
from .models import Task
from .models import Note


class TaskAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'study_type', 'start_date',
                    'end_date', 'completed', 'assignment')
    list_display_links = ('id', 'title',)
    list_filter = ('study_type', 'assignment')
    list_editable = ('study_type',)

    list_per_page = 25


class NoteAdmin(admin.ModelAdmin):
    list_display = ('id', 'text', 'task')
    list_filter = ('task',)
    list_editable = ('text',)

    list_per_page = 10


admin.site.register(Task, TaskAdmin)
admin.site.register(Note, NoteAdmin)
