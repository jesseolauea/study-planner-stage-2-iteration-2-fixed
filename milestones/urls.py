from django.urls import path
from . import views

urlpatterns = [
    path('validate', views.validate_milestone, name='validate_milestone'),
]