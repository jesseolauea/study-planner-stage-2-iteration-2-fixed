from datetime import datetime

from django.shortcuts import render
from django.utils.datastructures import MultiValueDictKeyError
from django.http import JsonResponse

from .models import Activity

# Validation


def validate_activity(request):
    from tasks.models import Task

    errors = {
        'name': None,
        'contribution': None,
        'task': None,
        'atype': None
    }

    name = str(request.POST['name']).strip()
    contribution = int(request.POST['contribution'])
    try:
        study_type = str(request.POST['atype']).upper()
    except:
        study_type = None
    associated_tasks = request.POST.getlist('task')

    # Name
    if len(name) > 80 or len(name) < 2:
        errors.update({'name': 'Name must be between 2 and 80 characters'})

    # Contribution
    if contribution < 1:
        errors.update({'contribution': 'Contribution needs to be at least 1'})

    # Type / Associated Tasks
    tasks = []
    for task_id in associated_tasks:
        task = Task.objects.get(pk=task_id)
        tasks.append(task)

    if study_type:
        for task in tasks:
            if task.study_type != study_type:
                errors.update(
                    {'task': 'Tasks need to be of the specified type'})

    return JsonResponse(errors)
