from django.contrib import admin
from .models import Activity, ActivityLogs


class ActivityAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'contribution', 'assignment')
    list_display_links = ('id', 'name')
    list_filter = ('assignment',)

    list_per_page = 25


admin.site.register(Activity, ActivityAdmin)


# class ActivityLogAdmin(admin.ModelAdmin):
#     list_display = ('id', 'activity_id',
#                     'name', 'contribution', 'time_complete')
#     list_display_links = ('id', 'name')
#     list_filter = ('activity_id',)

#     list_per_page = 25


# admin.site.register(ActivityLogs, ActivityLogAdmin)
