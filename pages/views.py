from django.shortcuts import render, redirect
from django.contrib import auth
from django.contrib.auth.models import User
from django.http import JsonResponse
from accounts.models import Account

from constants.types import ACCOUNT_TYPES

# Empty errors & user_input dictionaries
errors = {}
user_input = {}
errors_login = {}
user_input_login = {}
# register_attempted = False


def index(request):
    # Check if user is logged in
    user = auth.get_user(request)
    if user is not None and user.is_authenticated:
        return redirect('/profiles/{}'.format(user.id))
    else:
        return render(request, 'pages/index.html')


def about(request):
    # Check if user is logged in
    user = auth.get_user(request)
    if user is not None and user.is_authenticated:
        return redirect('/profiles/{}'.format(user.id))
    else:
        return render(request, 'pages/about.html')


def register(request):
    if request.method == 'POST':
        # register_attempted = True
        errors.clear()
        user_input.clear()

        # Get user input
        account_type = str(request.POST['type']).upper()
        name = request.POST['name']
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        confirm = request.POST['confirm']

        # Allow user input to persist
        user_input.update({'name': name})
        user_input.update({'username': username})
        user_input.update({'email': email})
        user_input.update({'password': password})
        user_input.update({'confirm': confirm})

        # Extract first name and last name from full name
        try:
            name = str(name).split(' ', 1)
            first_name = name[0]
            last_name = name[1]
        except:
            errors.update({'name': 'Last name is required'})

        # Passwords
        if password != confirm:
            errors.update({'confirm': 'Passwords do not match'})

        # Username
        if User.objects.filter(username=username).exists():
            errors.update({'username': 'Username is already taken'})

        # Email
        if User.objects.filter(email=email).exists():
            errors.update({'email': 'Email is already in use'})

        # Unsuccessful & Successful paths
        if len(errors.keys()) > 0:
            return redirect('register')
        else:
            # Create User
            user = User.objects.create_user(
                username,
                email=email,
                password=password,
                first_name=first_name,
                last_name=last_name
            )
            # Log user in
            user.save()
            auth.login(request, user)

            # Go to dashboard or teacher area
            account = Account.objects.get(user=user)
            account.account_type = account_type
            account.save()

            if account.account_type == 'STUDENT':
                return redirect('/profiles/{}'.format(user.id))
            else:
                return redirect('/teacher/{}'.format(user.id))
    else:
        context = {
            'types': ACCOUNT_TYPES,
            'errors': errors,
            'input': user_input
        }

        # Check if user is logged in
        user = auth.get_user(request)
        if user is not None and user.is_authenticated:
            return redirect('/profiles/{}'.format(user.id))
        else:
            return render(request, 'pages/register.html', context)


def login(request):
    if request.method == 'POST':
        errors_login.clear()
        user_input_login.clear()

        # Get user input
        username = request.POST['username']
        password = request.POST['password']

        # Allow user input to persist
        user_input_login.update({'username': username})
        user_input_login.update({'password': password})

        # Username
        user = None
        if not User.objects.filter(username=username).exists():
            errors_login.update(
                {'username': 'No user exists with this username'})
        else:
            # Check password is correct
            user = auth.authenticate(username=username, password=password)
            if user is None:
                errors_login.update({'password': 'Password is incorrect'})

        # Unsuccessful/Successful paths
        if len(errors_login.keys()) > 0:
            return redirect('login')
        else:
            auth.login(request, user)

            # Find account type
            account = Account.objects.get(user=user)

            if account.account_type == 'STUDENT':
                return redirect('/profiles/{}'.format(user.id))
            else:
                return redirect('/teacher/{}'.format(account.user.id))
    else:
        context = {
            'errors': errors_login,
            'input': user_input_login
        }

        # Check if user is logged in
        user = auth.get_user(request)
        if user is not None and user.is_authenticated:
            return redirect('/profiles/{}'.format(user.id))
        else:
            return render(request, 'pages/login.html', context)


def logout(request):
    if request.method == 'POST':
        auth.logout(request)
        return redirect('index')

# Validation


def validate_register(request):
    _errors = {
        'name': None,
        'username': None,
        'email': None,
        'password': None,
        'confirm': None
    }

    name = request.POST['name']
    username = request.POST['username']
    email = request.POST['email']
    password = request.POST['password']
    confirm = request.POST['confirm']

    try:
        name = str(name).split(' ', 1)
        first_name = name[0]
        last_name = name[1]
    except:
        _errors.update({'name': 'Last name is required'})

    # Passwords
    if password != confirm:
        _errors.update({'confirm': 'Passwords do not match'})

    # Username
    if User.objects.filter(username=username).exists():
        _errors.update({'username': 'Username is already taken'})

    # Email
    if User.objects.filter(email=email).exists():
        _errors.update({'email': 'Email is already in use'})

    return JsonResponse(_errors)


def validate_login(request):
    _errors = {
        'username': None,
        'password': None
    }

    username = request.POST['username']
    password = request.POST['password']

    # Username
    user = None
    if not User.objects.filter(username=username).exists():
        _errors.update(
            {'username': 'No user exists with this username'})
    else:
        # Check password is correct
        user = auth.authenticate(username=username, password=password)
        if user is None:
            _errors.update({'password': 'Password is incorrect'})

    return JsonResponse(_errors)
