from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('about', views.about, name='about'),
    path('register', views.register, name='register'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
    path('validate/register', views.validate_register, name='validate_register'),
    path('validate/login', views.validate_login, name='validate_login')
]
