[33mcommit eed6a5a40081b97a38279c7122bab4fb810ed1dd[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Author: Jesse Ola (CMP - Student) <sgj17quu@UEA.AC.UK>
Date:   Mon May 13 17:24:44 2019 +0100

    Started adding the teacher area

[33mcommit 583d129256da1ab43be213cc877b1241b042e773[m
Author: Jesse Ola (CMP - Student) <sgj17quu@UEA.AC.UK>
Date:   Mon May 13 16:20:59 2019 +0100

    Extra work

[33mcommit bf8fd811d5f4e0852407aadd2dd4f8452d7853eb[m
Author: Jesse Ola (CMP - Student) <sgj17quu@UEA.AC.UK>
Date:   Mon May 13 15:37:33 2019 +0100

    Merged activities/milestones

[33mcommit 4a1f80afbd2c502bcf26ad6ef31122875a7cb260[m
Author: Jesse Oladimeji <Jesse@eduroam-252-224.uea.ac.uk>
Date:   Mon May 13 12:00:46 2019 +0100

    More

[33mcommit 149c819a19ae3a1cb91a45e279bb29e6a94e4801[m
Author: Jesse Oladimeji <Jesse@jesses-air.home>
Date:   Mon May 13 00:04:02 2019 +0100

    loading from semester file works now

[33mcommit a78fbab8ae8d5674b28e725a41861615431359a8[m
Author: Jesse Oladimeji <Jesse@jesses-air.home>
Date:   Sun May 12 16:28:18 2019 +0100

    More

[33mcommit ad55d69784ada384d194adc7794f4ab98e5b1ce4[m
Author: Jesse Oladimeji <Jesse@eduroam-248-61.uea.ac.uk>
Date:   Sat May 11 19:41:31 2019 +0100

    More assingment/task work

[33mcommit b07a0f1cc6ba7a614312c63f9a8bdd36f8483bc2[m
Author: Jesse Oladimeji <Jesse@jesses-air.home>
Date:   Sat May 11 13:27:04 2019 +0100

    Extra work on assignment page

[33mcommit 999227545eea298221805fc8ff15311e08e02ddd[m
Author: Jesse Oladimeji <Jesse@jesses-air.home>
Date:   Sat May 11 01:41:35 2019 +0100

    Adding tasks is possible

[33mcommit 192fa7bdb01a7120817eeea118bdf0715c00d3c5[m
Author: Jesse Oladimeji <Jesse@jesses-air.home>
Date:   Fri May 10 23:05:15 2019 +0100

    Work on the assignment area

[33mcommit 8a69245f5ab812e984e655b431b2f2c8cd0feb8c[m
Author: Jesse Oladimeji <Jesse@Jesses-MacBook-Air.local>
Date:   Thu Apr 4 23:54:58 2019 +0100

    Deadline section updated

[33mcommit 5763545ff8499028b5597ab4f67ece4ceeef1d96[m
Author: Jesse Oladimeji <Jesse@Jesses-MacBook-Air.local>
Date:   Thu Apr 4 18:27:06 2019 +0100

    Creation of study profiles (without semester file)

[33mcommit 3f83e4b8286d1be8dd238986a58a896acb6cea8f[m
Author: Jesse Oladimeji <Jesse@Jesses-MacBook-Air.local>
Date:   Wed Apr 3 19:40:01 2019 +0100

    Piechart added

[33mcommit 66831495766486c6758ba622ac838b8ef2284178[m
Author: Jesse Oladimeji <Jesse@Jesses-MacBook-Air.local>
Date:   Tue Apr 2 15:19:11 2019 +0100

    improved the calendar

[33mcommit 45f4ac8ff7ec42e8ea9528087933efd58b40718b[m
Author: Jesse Oladimeji <Jesse@Jesses-MacBook-Air.local>
Date:   Tue Apr 2 02:40:46 2019 +0100

    More dashboard

[33mcommit 0ff012d7193c7a075c7b5dd002386ff1b1ad9786[m
Author: Jesse Oladimeji <Jesse@Jesses-MacBook-Air.local>
Date:   Mon Apr 1 23:49:22 2019 +0100

    More dashboard work

[33mcommit 83b444513bcd85a4209f15dfac7c355700a57db2[m
Author: Jesse Oladimeji <Jesse@Jesses-MacBook-Air.local>
Date:   Mon Apr 1 14:04:47 2019 +0100

    Dashboard work

[33mcommit d62aafe04a3776abf8d399f222e423d83efdfafe[m
Author: Jesse Oladimeji <Jesse@jesses-air.home>
Date:   Fri Mar 29 18:55:38 2019 +0000

    Registration

[33mcommit 134b8d91c0c92e14a6df33c494bb6ed6d79590b3[m
Author: Jesse Oladimeji <Jesse@jesses-air.home>
Date:   Thu Mar 28 21:13:35 2019 +0000

    Basic homepage with links to dummy pages

[33mcommit 2e74fca36ecafed5be5fd5b2684139b203d58c08[m
Author: Jesse Oladimeji <Jesse@eduroam-250-240.uea.ac.uk>
Date:   Thu Mar 28 16:39:14 2019 +0000

    Started basic design

[33mcommit 5b594bf697ea8e769a6cf52c1285edda234dbfe2[m
Author: Jesse Oladimeji <Jesse@eduroam-250-240.uea.ac.uk>
Date:   Thu Mar 28 15:04:18 2019 +0000

    Inclusion of default Django user model

[33mcommit c3f4585a094c661e74bcae0776f2ff374916fd3d[m
Author: Jesse Oladimeji <Jesse@eduroam-250-240.uea.ac.uk>
Date:   Thu Mar 28 15:03:17 2019 +0000

    Inclusion of default Django user model

[33mcommit 930e0a18311a78e6c98d99878c38d570c0dc35ef[m
Author: Jesse Oladimeji <Jesse@eduroam-250-240.uea.ac.uk>
Date:   Thu Mar 28 14:00:41 2019 +0000

    Stage One fields have been replicated

[33mcommit b87128f49f62af5fc9382e0587e746fb2e42fc80[m
Author: Jesse Oladimeji <Jesse@eduroam-250-240.uea.ac.uk>
Date:   Thu Mar 28 13:59:10 2019 +0000

    Stage One model fields have been replicated

[33mcommit e135c555f4da9ddad9b6a3d132aabe5e18d75506[m
Author: Jesse Oladimeji <Jesse@jesses-air.home>
Date:   Thu Mar 28 10:41:41 2019 +0000

    Initial Commit
