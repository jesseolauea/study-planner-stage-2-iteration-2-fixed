from django.shortcuts import render, redirect
from django.contrib import auth
from django.contrib.auth.models import User
from django.http import JsonResponse
from .models import Account

from constants.colours import MODULE_COLOURS, MODULE_STYLES, MODULE_ICONS
from constants.types import STUDY_TYPES, ASSIGNMENT_TYPES


def teacher(request, teacher_id):
    # User account
    user = User.objects.get(pk=teacher_id)
    account = Account.objects.get(user=user)
    modules = []

    for mod in account.modules.all():
        modules.append(mod)

    # Assign module colours
    for mod in modules:
        mod.colour = MODULE_COLOURS[modules.index(
            mod) % len(MODULE_COLOURS)]

        mod.style = MODULE_STYLES[modules.index(
            mod) % len(MODULE_STYLES)]

    # Assign module icons
    for mod in modules:
        mod.icon = MODULE_ICONS[modules.index(
            mod) % len(MODULE_ICONS)]

    # Make sure the user is actually a teacher
    if account.account_type != 'TEACHER':
        auth.logout(request)
        return redirect('login')

    # Make sure user has permissions
    if auth.get_user(request).id != teacher_id:
        auth.logout(request)
        return redirect('login')

    context = {
        'teacher': user,
        'modules': modules,
        'assignment_types': ASSIGNMENT_TYPES,
        'types': STUDY_TYPES
    }

    return render(request, 'accounts/teacherArea.html', context)


# Validation for when a student attempts to share a module with a teacher
def validate_teacher(request):
    errors = {
        'username': None
    }

    username = str(request.POST['username']).strip()

    # Check Existence of Account
    try:
        user = User.objects.get(username=username)
        account = Account.objects.get(user=user)

        # Check account type
        if account.account_type != 'TEACHER':
            errors.update({'username': 'This user is not a teacher'})
    except User.DoesNotExist:
        errors.update({'username': 'No user exists with this username'})

    return JsonResponse(errors)

