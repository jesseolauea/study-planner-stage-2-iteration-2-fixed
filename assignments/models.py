from django.db import models
from datetime import datetime
from modules.models import Module
from constants.types import ASSIGNMENT_TYPES


class Assignment(models.Model):
    module = models.ForeignKey(Module, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    assignment_type = models.CharField(max_length=50, choices=ASSIGNMENT_TYPES)
    deadline = models.DateTimeField()
    weighting = models.IntegerField(default=0)

    # Return assignment type, but lowercase
    @property
    def assignment_type_fmt(self):
        return str(self.assignment_type).lower().capitalize()

    # Calculate time spent on an assignment
    @property
    def time_spent(self):
        from tasks.models import Task

        time = 0
        for task in Task.objects.filter(assignment=self):
            time += task.time_spent

        return time

    # Return the tasks for an assignment
    @property
    def tasks(self):
        from tasks.models import Task

        return Task.objects.filter(assignment=self).order_by('-start_date')

    # Return the milestones for an assignment
    @property
    def milestones(self):
        from milestones.models import Milestone

        return Milestone.objects.filter(assignment=self).order_by('deadline')

    # Return the activities for an assignment
    @property
    def activities(self):
        from activities.models import Activity

        return Activity.objects.filter(assignment=self)

    # Return the activity log for an assignment
    @property
    def activity_log(self):
        from activities.models import ActivityLogs

        log = []

        for activity in self.activities:
            for log_item in ActivityLogs.objects.filter(activity_id=activity).order_by('-time_complete'):
                log.append(log_item)

        return log

    def __str__(self):
        return self.title
