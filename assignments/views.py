from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import auth
from django.http import JsonResponse
from django.contrib.auth.models import User
from datetime import datetime, timedelta

from django.utils.datastructures import MultiValueDictKeyError

from constants.types import STUDY_TYPES, ASSIGNMENT_TYPES

from .models import Assignment
from tasks.models import Task


@login_required(login_url='login')
def assignment(request, assignment_id):
    # assignment = Assignment.objects.get(id=assignment_id)
    _assignment = get_object_or_404(Assignment, pk=assignment_id)

    # Make sure the user has permission to view this page
    if auth.get_user(request).id != _assignment.module.profile.account.id:
        # auth.logout(request)
        return redirect('index')

    context = {
        'assignment': _assignment,
        'types': STUDY_TYPES,
        'assignment_types': ASSIGNMENT_TYPES,
        'next_week': datetime.now() + timedelta(days=7) - timedelta(days=7*8)
    }

    return render(request, 'assignments/assignment.html', context)


@login_required(login_url='login')
def create_task(request, assignment_id):
    from accounts.models import Account

    if request.method == 'POST':
        assignment = Assignment.objects.get(id=assignment_id)
        title = request.POST['title']
        description = request.POST['description']
        study_type = str(request.POST['studytype']).upper()
        target = request.POST['target']
        dependencies = request.POST.getlist('dependencies', [])
        start_date = request.POST['start']
        end_date = request.POST['end']

        # Create the new object
        task = Task(
            assignment=assignment,
            title=title,
            description=description,
            study_type=study_type,
            target=target,
            start_date=start_date,
            end_date=end_date
        )

        task.save()

        for dep in dependencies:
            task.add_dependency(dep)

        if Account.objects.get(user=auth.get_user(request)).account_type == 'STUDENT':
            return redirect('/assignments/{}'.format(assignment.id))
        else:
            return redirect('/teacher/{}'.format(auth.get_user(request).id))


@login_required(login_url='login')
def add_note(request, assignment_id, task_id):
    from tasks.models import Note

    data = {
        'text': None
    }

    text = request.POST['text']

    task = Task.objects.get(pk=task_id)

    # Check that user has permission
    if auth.get_user(request).id != task.assignment.module.profile.account.id:
        auth.logout(request)
        return redirect('index')

    # Add the note
    note = Note(task=task, text=text)
    note.save()

    data.update({'text': note.text})

    return JsonResponse(data)


@login_required(login_url='login')
def do_activity(request, assignment_id):
    from activities.models import Activity, ActivityLogs

    contribution = None
    time_taken = None

    # POST REQUEST
    if request.method == 'POST':

        # save to logs
        contribution = int(request.POST['contribution'])
        time_taken = int(request.POST['time_taken'])
        activity_id = int(request.POST['activityid'])

        activity = get_object_or_404(Activity, pk=activity_id)

        tasks = activity.associated_tasks.all()

        a = ActivityLogs(
            activity_id=Activity.objects.get(pk=activity_id),
            name=Activity.objects.get(pk=activity_id).name,
            time_taken=time_taken,
            contribution=contribution
        )
        a.save()

        # increment time dedicated to activity as well as contribution to tasks
        actToSave = Activity.objects.get(pk=activity_id)
        actToSave.do_activity(time_taken, contribution)
        actToSave.contribution = contribution
        actToSave.save()

    return redirect('/assignments/{}'.format(assignment_id))


@login_required(login_url='login')
def view_activity(request):
    name = "a5"

    # data to pass onto the web page
    context = {
        'activity': name
    }

    return render(request, 'profiles/addActivityModal.html', context)


@login_required(login_url='login')
def add_activity(request, assignment_id):
    from activities.models import Activity

    # NEED TO ADD BASED UPON ASSIGNMENT

    # boolean value to determine that the study types match
    types_match = True

    # get activities
    activities = [activity for activity in Activity.objects.all()]

    # Get tasks
    tasks = [task for task in Task.objects.all()]

    # POST REQUEST
    if request.method == 'POST':

        name = request.POST['name']
        contribution = int(request.POST['contribution'])

        # only returns selected tasks
        got_tasks = request.POST.getlist('task')

        # only returns the selected study type
        got_type = request.POST.getlist('atype')

        # ensure that the study type matches the types of the tasks

        for task_id in got_tasks:

            task_from_id = Task.objects.get(id=task_id)

            if(got_type[0].upper() != task_from_id.study_type):
                types_match = False

        # add activity information to database
        if(types_match is True):
            a = Activity(
                assignment=Assignment.objects.get(pk=assignment_id),
                name=name,
                contribution=contribution,
                time_taken=0,
                study_type=got_type[0].upper()
            )
            a.save()

            for task_id in got_tasks:
                task_from_id = Task.objects.get(id=task_id)
                a.associated_tasks.add(task_from_id)

            print(a)

    # data to pass onto the web page
    context = {
        'activities': activities,
        'tasks': tasks,
        'types': STUDY_TYPES,
        'validateType': types_match
    }

    return redirect('/assignments/{}'.format(assignment_id))


# adding a milestone
def add_milestone(request, assignment_id):
    from milestones.models import Milestone

    tasks = [task for task in Task.objects.all()]

    # boolean value to determine if deadline is ahead of tasks to complete deadline
    deadline_valid = True

    if request.method == "POST":
        name = request.POST['name']
        got_tasks = request.POST.getlist('task')

        date = request.POST['date']
        time = request.POST['time']

        concat = date + ' ' + time
        format_date_time = datetime.strptime(concat, '%Y-%m-%d %H:%M')

        task_count = 0

        # iterate through all tasks
        for task_id in got_tasks:
            task_from_id = Task.objects.get(id=task_id)
            task_from_id.end_date = task_from_id.end_date.replace(tzinfo=None)

            if task_from_id.end_date.replace(tzinfo=None) > format_date_time:
                deadline_valid = False

            task_count += 1

        if deadline_valid is True:
            m = Milestone(assignment=Assignment.objects.get(pk=assignment_id), name=name,
                          deadline=format_date_time)
            m.save()

            count = 0

            for task_id in got_tasks:

                task_from_id = Task.objects.get(id=task_id)
                m.tasks_to_complete.add(task_from_id)
                m.set_progress()

                if task_from_id.completed is True:
                    count += 1

            # set progress
            progress = count / task_count
            if progress == 1.0:
                m.complete = True
                m.save()

            m.progress = progress

            m.save()

    context = {'tasks': tasks,
               'validDeadline': deadline_valid}

    return redirect('/assignments/{}'.format(assignment_id))


@login_required(login_url='login')
def delete_task(request, assignment_id):
    if request.method == 'POST':
        task_id = int(request.POST['taskid'])

        task = Task.objects.get(pk=task_id)

        # Check user has permission to delete the task
        if auth.get_user(request).id != task.assignment.module.profile.account.id:
            auth.logout(request)
            return redirect('login')

        # Delete the task
        task.delete()

        return redirect('/assignments/{}'.format(assignment_id))


@login_required(login_url='login')
def delete_milestone(request, assignment_id):
    from milestones.models import Milestone

    if request.method == 'POST':
        milestone_id = int(request.POST['milestoneid'])

        milestone = Milestone.objects.get(pk=milestone_id)

        # Check user has permission to delete the milestone
        if auth.get_user(request).id != milestone.assignment.module.profile.account.id:
            auth.logout(request)
            return redirect('login')

        # Delete the task
        milestone.delete()

        return redirect('/assignments/{}'.format(assignment_id))


@login_required(login_url='login')
def delete_activity(request, assignment_id):
    from activities.models import Activity

    if request.method == 'POST':
        activity_id = int(request.POST['activityid'])

        activity = Activity.objects.get(pk=activity_id)

        # Check user has permission to delete the activity
        if auth.get_user(request).id != activity.assignment.module.profile.account.id:
            auth.logout(request)
            return redirect('login')

        # Delete the task
        activity.delete()

        return redirect('/assignments/{}'.format(assignment_id))


@login_required(login_url='login')
def delete_assignment(request, assignment_id):
    if request.method == 'POST':
        assignment = Assignment.objects.get(pk=assignment_id)

        # Check user has permission to delete the assignment
        if auth.get_user(request).id != assignment.module.profile.account.id:
            auth.logout(request)
            return redirect('login')

        # Delete the assignment
        assignment.delete()

        return redirect('/profiles/{}'.format(auth.get_user(request).id))

# viewing a milestone, allows the completion of tasks


@login_required(login_url='login')
def edit_assignment(request, assignment_id):
    if request.method == 'POST':
        title = request.POST['title']
        assignment_type = str(request.POST['atype']).upper()
        deadline = request.POST['deadline']
        weighting = request.POST['weighting']

        assignment = Assignment.objects.get(pk=assignment_id)

        # Check user has permission to edit the assignment
        if auth.get_user(request).id != assignment.module.profile.account.id:
            auth.logout(request)
            return redirect('login')

        assignment.title = title
        assignment.assignment_type = assignment_type
        assignment.deadline = deadline
        assignment.weighting = weighting
        assignment.save()

        return redirect('/assignments/{}'.format(assignment_id))


@login_required(login_url='login')
def edit_task(request, assignment_id):
    if request.method == 'POST':
        task_id = request.POST['taskid']
        title = request.POST['title']
        description = request.POST['description']
        target = int(request.POST['target'])
        start_date = request.POST['start']
        end_date = request.POST['end']

        task = Task.objects.get(pk=task_id)

        # Check user has permission to edit the task
        if auth.get_user(request).id != task.assignment.module.profile.account.id:
            auth.logout(request)
            return redirect('login')

        task.title = title
        task.description = description
        task.target = target
        task.start_date = start_date
        task.end_date = end_date

        if task.amount_complete > task.target:
            task.amount_complete = task.end_date

        if task.amount_complete < task.target:
            task.completed = False

        task.save()

        return redirect('/assignments/{}'.format(assignment_id))


@login_required(login_url='login')
def edit_activity(request, assignment_id):
    from activities.models import Activity

    if request.method == 'POST':
        activity_id = request.POST['activityid']
        name = request.POST['name']
        contribution = request.POST['contribution']
        associated_tasks = request.POST.getlist('task')

        activity = Activity.objects.get(pk=activity_id)

        # Check user has permission to edit the activity
        if auth.get_user(request).id != activity.assignment.module.profile.account.id:
            auth.logout(request)
            return redirect('login')

        activity.name = name
        activity.contribution = contribution

        for task_id in associated_tasks:
            task_from_id = Task.objects.get(id=task_id)
            activity.associated_tasks.add(task_from_id)

        activity.save()

        return redirect('/assignments/{}'.format(assignment_id))


@login_required(login_url='login')
def edit_milestone(request, assignment_id):
    from milestones.models import Milestone

    if request.method == 'POST':
        milestone_id = int(request.POST['milestoneid'])
        name = request.POST['name']
        got_tasks = request.POST.getlist('task')

        date = request.POST['date']
        time = request.POST['time']

        concat = date + ' ' + time
        format_date_time = datetime.strptime(concat, '%Y-%m-%d %H:%M')

        m = Milestone.objects.get(pk=milestone_id)

        # Check user has permission to delete the assignment
        if auth.get_user(request).id != m.assignment.module.profile.account.id:
            auth.logout(request)
            return redirect('login')

        # Modify the milestone
        m.name = name
        m.deadline = format_date_time

        for task_id in got_tasks:
            task_from_id = Task.objects.get(id=task_id)
            m.tasks_to_complete.add(task_from_id)
            m.set_progress()

        m.save()

        return redirect('/assignments/{}'.format(assignment_id))

# def view_milestone(request, mil_name):
#     from activities.models import Activity

#     # get all activities
#     activities = Activity.objects.all()

#     # to filter out relevant activities
#     activitiesToShow = []

#     m = Milestone.objects.get(name=mil_name)

#     tasks = m.tasks_to_complete.all()

#     for activity in activities:
#         for task in tasks:
#             if task in activity.associated_tasks.all():
#                 activitiesToShow.append(activity)

#     context = {'activities': activitiesToShow,
#                'tasks': tasks,
#                'milestone': m}
#     return render(request, 'profiles/viewMilestoneModal.html', context)
